import appdb
import unittest

class TestWeather(unittest.TestCase):
    def setUp(self):
        self.app = appdb.app.test_client()
        self.app.testing = True

    def test_result_found(self):
        result = self.app.get('/weather/London/20180524/1800')
        self.assertEqual(result.status_code, 200)

    def test_result_not_found(self):
        result = self.app.get('/weather/London/21000523/1800')
        self.assertEqual(result.status_code, 404)

    def test_result_only_temperature(self):
        result = self.app.get('/weather/London/20180524/1800/temperature')
        content = result.data.decode('utf-8')
        self.assertNotIn('humidity',content)
        self.assertNotIn('pressure',content)
        self.assertNotIn('description',content)
        self.assertIn('temperature',content)

    def test_result_only_pressure(self):
        result = self.app.get('/weather/London/20180524/1800/pressure')
        content = result.data.decode('utf-8')
        self.assertNotIn('humidity',content)
        self.assertNotIn('temperature',content)
        self.assertNotIn('description',content)
        self.assertIn('pressure',content )

    def test_result_only_humidity(self):
        result = self.app.get('/weather/London/20180524/1800/humidity')
        content = result.data.decode('utf-8')
        self.assertNotIn('pressure',content)
        self.assertNotIn('temperature',content)
        self.assertNotIn('description',content)
        self.assertIn('humidity',content )

    def test_bad_date_param(self):
        result = self.app.get('/weather/London/2018052P/1800')
        self.assertEqual(result.status_code, 400) 

    def test_bad_time_param(self):
        result = self.app.get('/weather/London/20180523/18AA')
        self.assertEqual(result.status_code, 400)    

if __name__ == '__main__':
    unittest.main()