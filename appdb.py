import os
from flask import Flask, abort, request, jsonify, g, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, DateTime, Float,exc
from sqlalchemy.ext.declarative import declarative_base
import requests
import json
import calendar
import time
import datetime

app = Flask(__name__)
app.config['OWM_URL']="https://api.openweathermap.org/data/2.5/forecast"
app.config['APPID']='47688db2c9b0083b58310553ec424da5'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.url_map.strict_slashes = False

db = SQLAlchemy(app)

pattern = '%Y%m%d %H%M'
HUMIDITY = 'humidity'
TEMPERATURE = 'temperature'
PRESSURE = 'pressure'
DESCRIPTION = 'description'

def convert_to_datetime_utc(date,hour_minutes):
    '''convert a give date and time in UTC into a seconds since January 1, 1970
        :param date: format YYYYmmdd
        :hour_minutos: format HHMM
        :returns: seconds since January 1, 1970 at 00:00:00 GMT
    '''
    query_date='{} {}'.format(date,hour_minutes)
    return calendar.timegm(time.strptime(query_date, pattern))
def check_arguments(city, date, hour_minutes):
    if city.isalpha()==False:
        return (False, 'City should be alfabetic only')
    if len(date)!=8 or date.isdigit()==False:
        return (False,'Date should have the format YYYYmmdd')
    if len(hour_minutes)!=4 or hour_minutes.isdigit()==False:
        return (False,'Time should have the format HHDD')
    return (True, '')

def prepare_forecast(forecast, fields):
    show_all=False
    data={}
    if len(fields)==0:
        show_all=True
    if DESCRIPTION in fields or show_all: data[DESCRIPTION]=forecast.description
    if TEMPERATURE in fields or show_all: data[TEMPERATURE]='{}C'.format(forecast.temperature)
    if PRESSURE in fields or show_all: data[PRESSURE]=str(forecast.pressure)
    if HUMIDITY in fields or show_all: data[HUMIDITY]='{}%'.format(forecast.humidity)   
    return data

def save_forecast(result, city,dt):
    external_forecast = Forecast(city=city, datetime=datetime.datetime.utcfromtimestamp(dt))
    external_forecast.description = result['weather'][0]['description']
    external_forecast.humidity=result['main']['humidity']
    external_forecast.pressure=result['main']['pressure']
    external_forecast.temperature=result['main']['temp']

    db.session.add(external_forecast)
    try:
        db.session.commit()
    except exc.SQLAlchemyError:
        external_forecast=None

    return external_forecast

def handle_not_found(data,dt):
    data['status']='error'
    data['message']='No data for {}'.format(time.strftime("%Y-%m-%d %H:%M",time.gmtime(dt)))
    return (data, 404)

def handle_error(data, dt, city, status_code):
    data['status_code']=status_code
    data['message']='No data for {} in {}'.format(city, time.strftime("%Y-%m-%d %H:%M",time.gmtime(dt)))
    return (data, status_code)

def get_forecast_from_repository(city,dt):
    try:
        forecast=Forecast.query.filter(Forecast.city==city).filter(Forecast.datetime==datetime.datetime.utcfromtimestamp(dt)).one_or_none()
    except exc.SQLAlchemyError:
        forecast = None
    return forecast

def fetch_data(city, date, hour_minutes, fields):
    '''Fetches data from OpenWeatherMaps service
        :param city
        :param data: format YYYmmddd
        :param hour_minutes: format HHDD
        :param args: list of fields to return. If it is empty, returns all fields.
        :returns: all fields or just the ones asked'''
    data = {}
    city = city.title()
    status, message = check_arguments(city,date, hour_minutes)
    if status==False:
        data['message']=message
        return (data,400)

    dt = convert_to_datetime_utc(date,hour_minutes)
    
    internal_forecast=get_forecast_from_repository(city,dt)

    if internal_forecast is not None:
        data = prepare_forecast(internal_forecast,fields)
        return (data,200)

    url = app.config['OWM_URL'] #+'?appid='+app.config['APPID']+'&q='+city+"&units=metric"
    payload={'appid':app.config['APPID'],'q':city,'units':'metric'}
    try:
        response = requests.get(url,params=payload)
    except requests.exceptions.RequestException as e:
        return handle_error(dict(error=e),dt,city, e.response.status_code)
    forecast_found=None
    if response.status_code==200:
        content = response.json()
        items = content['list']
        for item in items:
            forecast = get_forecast_from_repository(city, int(item['dt']))
            if forecast is None:
                external_forecast = save_forecast(item,city, int(item['dt']))
            if item['dt']==int(dt):
                forecast_found=external_forecast
        if forecast_found is not None:
                data = prepare_forecast(forecast_found,fields)
                return (data,200)
        else:
            return handle_not_found(data, dt)
    else:
        return handle_error(data,dt,city, response.status_code)

class Forecast(db.Model):
    __tablename__ = 'forecast'
    city=Column(String(64),primary_key=True)
    datetime=Column(DateTime,primary_key=True)
    temperature=Column(Float)
    pressure=Column(Float)
    humidity=Column(Float)
    description=Column(String(255))

    def __init__(self, city,datetime):
        self.city = city
        self.datetime = datetime

@app.route('/weather/<string:city>/<string:date>/<string:hour_minutes>')
def get_weather(city, date, hour_minutes):
    data,status = fetch_data(city,date,hour_minutes,[])
    return jsonify(data),status

@app.route('/weather/<string:city>/<string:date>/<string:hour_minutes>/temperature')
def get_weather_temperature(city, date, hour_minutes):
    data,status = fetch_data(city,date,hour_minutes,[TEMPERATURE])
    return jsonify(data),status

@app.route('/weather/<string:city>/<string:date>/<string:hour_minutes>/humidity')
def get_weather_humidity(city, date, hour_minutes):
    data,status = fetch_data(city,date,hour_minutes,[HUMIDITY])
    return jsonify(data),status

@app.route('/weather/<string:city>/<string:date>/<string:hour_minutes>/pressure')
def get_weather_pressure(city, date, hour_minutes):
    data,status = fetch_data(city,date,hour_minutes,[PRESSURE])
    return jsonify(data),status

if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    app.run(host='0.0.0.0', port=5000, debug=True)