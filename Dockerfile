FROM python:3.6.5-alpine
RUN mkdir -p /opt/code
ADD . /opt/code
WORKDIR /opt/code
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["appdb.py"]