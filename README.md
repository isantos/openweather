# myopenweatherapi
Python/flask REST web service providing an API to query the data from www.openweathermap.org

# Installation
```bash
# clone the repository
~$ git clone https://gitlab.com/isantos/openweather.git
~$ pip install -r requirements.txt
~$ python appdb.py 
```
# Usage
To obtain a general summary of the weather of a city a given date and time you can query the API with the following format
```bash
http://<hostname>/weather/<city>/<date>/<time>
```
* Date :format _YYYmmdd_
* Time :format _HHMM_

For example,
```bash
http://<hostname>/weather/Manchester/20180525/0900
```
will return 
```json
{
    "description": "clear sky", 
    "temperature": "14.97C", 
    "pressure": "1008.01", 
    "humidity": "53%"
}
```
You can query only some of the fields appending it name at the end of the URL
For example,
```bash
http://<hostname>/weather/Manchester/20180525/0900/temperature
```
will return,
```json
{ 
    "temperature": "14.97C", 
}
```
You can also query *pressure* and *humidity*

If you ask for the weather of a unknown city or at time without date you will receive the following message
```json
{
    "status_code": 404, 
    "message": "No data for Manchester in 2018-05-25 10:00"
}
```

# Tests
The interface described above can be tested with
```bash
# clone the repository
~$ python TestWeather.py
```

# Docker 
You can test the service using a docker container with
```bash
~$ docker-compose up
```

# Demo
There is an online service demo [here](https://rocky-temple-74296.herokuapp.com/weather/Manchester/20180525/0900)